program pacmad;

uses crt,aplib,neo6502,neo6502math;

{$I globals.inc}
{$I assets/sprites.inc}

var
    pac: pacT;
    dir: byte;
    joy: byte;
    frame: byte;

    board: boardT absolute BOARDBASE;
    tileset: boardT absolute TILESETBASE;
    tmap:TTileMapHeader absolute TILESETBASE-sizeOf(TTileMapHeader);

    boardOffset: array [0..BOARDHEIGHT] of word;
    boardLastLine: byte;

    viewportTop: byte;
    viewportMoveDir: shortint;
    vscrollstep: shortint;
    vOffset: smallInt;
    bOffset: smallInt;

    levels: array [0..0] of word absolute LEVELS_ADDRESS;
    levelData: array [0..0] of byte;
    level: levelT;
    levelCount: byte;
    currentLevel: byte;

    ghost1, ghost2, ghost3, ghost4: ghostT;
    g: ^ghostT;
    ghosts: array [0..3] of pointer;
    ghostCount: byte;
    ghostFrame: byte;
    ghostSpritesheets: array [0..3] of pointer;

    pillTime: word;
    pillCounter: word;
    pillBonus: word;

    collider: byte;
    titleStage: byte;
    gameState: gameStateT;

    spawners: array [0..SPAWNERS_NUM - 1] of smallInt;
    spawnerCount: byte;
    spawnerActive: byte;
    spawnDelay: word;
    spawnCounter: word;

    bonusPosPicked:boolean;
    bonusX:byte;
    bonusY:byte;
    bonusCountdown:word;
    bonusDelay:word;

    score: cardinal;
    hiscore: cardinal;
    liveBonus: word;
    lives: byte;
    dotCount: word;

    s: string[60];
    i: byte;
    flag:boolean;
    k:char;
    
	startingLevel:byte;

    _STICK0:byte;

    palColor:^TPalColor;

    
{$R assets/asm_assets.rc}
{$I music.inc}

(********************************************** GUI ROUTINES ******************************************)

procedure DrawRect(x1,y1,x2,y2:word;c:byte);
begin
    NeoSetDefaults(0,c,1,1,0);
    NeoDrawRect(x1,y1,x2,y2);
end;

procedure DrawString(x,y:word;size:byte;c:byte;var s:string);
begin
    NeoSetDefaults(0,c,0,size,0);
    NeoDrawString(x,y,s);
end;

procedure DrawStringSh(x,y:word;size:byte;c:byte;var s:string);
var o:byte;
begin
    o := size shr 1 + 1;
    NeoSetDefaults(0,0,0,size,0);
    NeoDrawString(x+o,y+o,s);
    NeoSetColor(c); 
    NeoDrawString(x,y,s);
end;

procedure ReadStick;
begin
    _STICK0 := NeoGetJoy(0);
end;

function FireAPressed:boolean;
begin
    result := false;
    ReadStick;
    if (_STICK0 and 16) <> 0 then result := true;
end;

function FireBPressed:boolean;
begin
    result := false;
    ReadStick;
    if (_STICK0 and 32) <> 0 then result := true;
end;

procedure HideSprites;
begin
    for i:=0 to 5 do NeoHideSprite(i);
end;

procedure GetAtariCol(col:byte);
var off:word;
begin   
    off:=(col shl 1) + col;
    palColor := pointer(PALETTE_ADDRESS + off);
end;

procedure SetTileColor(c,r,g,b:byte;ontop:boolean);
var n:byte;
begin
    NeoSetPalette(c,r,g,b);
    if ontop then  for n:=0 to 15 do NeoSetPalette((n shl 4) + c,r,g,b);
end;

procedure SetSpriteColor(c,r,g,b:byte);
var n:byte;
begin
    for n:=0 to 15 do NeoSetPalette((c shl 4)+n,r,g,b);
end;

procedure SpriteCol(c,acol:byte);
begin
    GetAtariCol(acol);
    SetSpriteColor(c,palColor.r,palColor.g,palColor.b);
end;

procedure TileCol(c,acol:byte;top:boolean);
begin
    GetAtariCol(acol);
    SetTileColor(c,palColor.r,palColor.g,palColor.b,top);
end;

const TOP_OFFX = 9;

procedure ShowTopBar;
begin
    s:='Score:                Food:                Lives:';
    DrawString(TOP_OFFX,2,1,$f,s);
end;

procedure UpdateScore;
begin
    NeoStr(score, s);
    DrawRect(TOP_OFFX+36,2,TOP_OFFX+36+40,10,$8);
    DrawString(TOP_OFFX+36,2,1,$f,s);
end;

procedure LoadHiscore;
begin
    NeoLoad('pacmad.hsc',cardinal(@hiscore));
    if NeoMessage.error<>0 then hiscore:=0;
end;

procedure StoreHiscore;
begin
    NeoSave('pacmad.hsc',cardinal(@hiscore),4);
end;

procedure UpdateFood;
begin
    NeoStr(dotCount, s);
    DrawRect(TOP_OFFX+162,2,TOP_OFFX+162+40,10,8);
    DrawString(TOP_OFFX+162,2,1,$f,s);
end;

procedure UpdateLives;
begin
    NeoStr(lives, s);
    DrawRect(TOP_OFFX+294,2,319,10,$8);
    DrawString(TOP_OFFX+294,2,1,$f,s);
end;


(********************************************** BOARD ROUTINES ******************************************)


procedure PaintOnTileset(x, y, t: byte);
begin
    vOffset := boardOffset[y] + x;
    tileset[vOffset] := tiles[t];
end;

function GetBoard(x, y: byte): byte;
var pos: word;
begin
    pos := boardOffset[y] + x;
    result := board[pos];
end;

procedure SetBoard(x, y, v: byte);
var pos: word;
begin
    pos := boardOffset[y] + x;
    board[pos] := v;
    PaintOnTileset(x, y, v);
end;

function CanMoveTo(x, y: byte): boolean;
begin
    Result := true;
    if GetBoard(x, y) = TILE_VOID then exit(false);
    if x >= BOARDWIDTH then exit(false);
    if y >= BOARDHEIGHT then exit(false);
    if GetBoard(x, y) = TILE_SPAWNER then exit(false);
end;

function GetNeighbours(x, y: byte): byte;
var counter: byte;
begin
    counter := 0;
    result := 0;
    if canMoveTo(x-1, y) then begin  // left
        Inc(result, JOY_LEFT);
        Inc(counter);
    end;
    if canMoveTo(x+1, y) then begin
        Inc(result, JOY_RIGHT);    // right
        Inc(counter);
    end;
    if canMoveTo(x, y+1) then begin
        Inc(result, JOY_DOWN);    // down
        Inc(counter);
    end;
    if canMoveTo(x, y-1) then begin
        Inc(result, JOY_UP);    // up
        Inc(counter);
    end;
    counter := counter shl 4;
    result := result or counter;
end;

procedure AddScore(val:word);
begin
    Inc(score, val);
    Inc(liveBonus, val);
    if (liveBonus >= LIFE_BONUS) then begin
        Inc(lives);
        UpdateLives;
        Dec(liveBonus, LIFE_BONUS);
        Sfx(SFX_LIFE);
    end;
    UpdateScore;
end;

procedure CheckBoardAction;
var cell: byte;
begin
    cell := GetBoard(pac.x, pac.y);
    case cell of
        TILE_DOT: begin
            AddScore(10);
            if dotCount<>0 then begin
				Dec(dotCount);
				if dotCount=0 then Sfx(SFX_EXIT);
			end;
            UpdateFood;
            SetBoard(pac.x, pac.y, TILE_EMPTY);
        end;
        TILE_PILL: begin
            AddScore(50);
            SetBoard(pac.x, pac.y, TILE_EMPTY);
            gameState := GAME_ESCAPE;
            SpriteCol(4,COLOR_ESCAPE);
            pillCounter := pillTime;
            pillBonus := 0;
            PlaySong(1);
        end;
        TILE_WARP_LEFT: begin
            pac.x := 20;
        end;
        TILE_WARP_RIGHT: begin
            pac.x := 0;
        end;
        TILE_EXIT: begin
            if dotCount = 0 then begin
                gameState := GAME_WIN;
                frame := 0;
            end;
        end;
        TILE_BONUS, TILE_BONUS+1, TILE_BONUS+2: begin
            AddScore(500);
            bonusCountdown:=100;
            SetBoard(bonusX,bonusY,TILE_500);
            sfx(SFX_BONUS);
        end;
    end;
end;

procedure BoardRect(x, y, w, h, f: byte);
begin
    Dec(h);
    Dec(w);
    for i := x to x + w do begin
        SetBoard(i, y, f);
        SetBoard(i, y + h, f);
    end;
    if h > 0 then begin
        Dec(h);
        for i := y + 1 to y + h do begin
            SetBoard(x, i, f);
            SetBoard(x+w ,i, f);
        end;
    end;
end;

procedure PaintBoard;
var row, col, cell, t: byte;
    boffset, tile: word;
begin
    boffset := 0;
    boardLastLine := 0;
    dotCount := 0;
    for row := 0 to BOARDHEIGHT - 1 do begin
        for col := 0 to BOARDWIDTH - 1 do begin
            cell := board[boffset];
            if cell = TILE_VOID then begin
				t := GetNeighbours(col,row) and 15;
                tile := fences[t];
                if (tile = 1) and CanMoveTo(col+1, row+1) then tile := fences[16];
                if (tile = 4) and CanMoveTo(col+1, row+1) then tile := fences[17];
            
            end else begin
                tile := tiles[cell];
                if (cell = TILE_DOT) then Inc(dotCount);
                boardLastLine := row;
            end;
            tileset[boffset]:=tile;
            Inc(boffset);
        TileCol(1,boffset and $ff,false);
        end;
    end;
    Inc(boardLastLine,2);
    if GetBoard(level.exitX,level.exitY) = TILE_DOT then Dec(dotCount);
    SetBoard(level.exitX,level.exitY,TILE_EXIT);
end;

procedure ClearFeatures;
begin
    for i := 0 to SPAWNERS_NUM - 1 do spawners[i] := -1;
end;

procedure InitTables;
var row: byte;
    boffset: word;
begin
    boffset := 0;
    for row := 0 to BOARDHEIGHT - 1 do begin
        boardOffset[row] := boffset;
        Inc(boffset, BOARDWIDTH);
    end;
    ghosts[0] := @ghost1;
    ghosts[1] := @ghost2;
    ghosts[2] := @ghost3;
    ghosts[3] := @ghost4;
    hiscore := 0;
    levelCount := 0;
    repeat
        Inc(levelCount)
    until levels[levelCount] = 0;

	startingLevel := 0;
end;

procedure MoveViewport;
begin
    if (viewportMoveDir <> 0) then begin     // if vscroll in progress jump to another line
        vscrollstep := (vscrollstep + viewportMoveDir) and 7;
        if (vscrollstep = 7) and (viewportMoveDir = -1) then begin // if vscroll starts up
            viewportTop := viewportTop + viewportMoveDir;
        end;
        if (vscrollstep = 0) and (viewportMoveDir = 1) then begin // if vscroll ends
            viewportTop := viewportTop + viewportMoveDir;
        end;
    end;
end;

procedure BlinkExit;
begin
    if (frame and 4) = 0 then TileCol($06,level.colors[4],false)
    else TileCol($06,level.colors[0],false);
end;

procedure PickBonusPos;
var x,y:byte;
begin
    x:=NeoIntRandom(BOARDWIDTH);
    y:=NeoIntRandom(BOARDHEIGHT);
    if GetBoard(x,y) = TILE_EMPTY then begin
        bonusX := x;
        bonusY := y;
        bonusPosPicked := true;
    end;
end;

procedure RemoveBonus;
begin
    SetBoard(bonusX,bonusY,TILE_EMPTY);
    bonusPosPicked := false;
    bonusDelay := BONUS_DELAY * 50;
end;

procedure PlaceBonus;
begin
    i:=TILE_BONUS + NeoIntRandom(BONUS_COUNT);
    SetBoard(bonusX,bonusY,i);
    bonusCountdown := BONUS_COUNTDOWN * 50;
end;


{********************************************** GHOST ROUTINES ******************************************}


procedure ClearGhosts;
begin
    for i := 0 to GHOST_NUM-1 do begin
        g := ghosts[i];
        g^.dir := DIR_NONE;
        g^.deadCount := 0;
        NeoHideSprite(i+1);
    end;
    ghostCount := 0;
end;

procedure SpawnGhost(ghostNumber, x, y: byte);
var dir, nx, ny: byte;
begin
    g := ghosts[ghostNumber];
    g^.x := x;
    g^.y := y;
    repeat
        nx := x;
        ny := y;
        dir := Random(4);
        if (dir = DIR_UP) then Dec(ny);
        if (dir = DIR_DOWN) then Inc(ny);
        if (dir = DIR_LEFT) then Dec(nx);
        if (dir = DIR_RIGHT) then Inc(nx);
    until CanMoveTo(nx, ny);
    g^.dir := dir;
    g^.deadCount := 0;
    g^.despawn := 0;
    ghostSpritesheets[ghostNumber] := ghost_sprites[dir];
    g^.step := 0;
    g^.level := level.ai[ghostNumber];
    Inc(ghostCount);
end;

procedure KillGhost(ghostNumber: byte);
begin
    g := ghosts[ghostNumber];
    g^.dx := g^.x;
    if g^.dx = 0 then g^.dx := 1
    else if g^.dx = 20 then g^.dx := 19;
    g^.dy := g^.y;
    g^.deadCount := 150;
    if pillBonus = 100 then g^.reward := TILE_100;
    if pillBonus = 200 then g^.reward := TILE_200;
    if pillBonus = 300 then g^.reward := TILE_300;
    if pillBonus = 400 then g^.reward := TILE_400;
    if pillBonus = 500 then g^.reward := TILE_500;
    PaintOnTileset(g^.dx, g^.dy, g^.reward);
    NeoHideSprite(ghostNumber + 1);
end;

procedure DespawnGhost(ghostNumber: byte);
begin
    g := ghosts[ghostNumber];
    g^.deadCount := 0;
    g^.dir := DIR_NONE;
    Dec(ghostCount);
    NeoHideSprite(ghostNumber + 1);
end;

function RandomDir(mask: byte): byte;
var a:array[0..3] of byte;
	ai:byte;
begin
	ai:=0;
	for result:=0 to 3 do 
		if dirs[result] and mask <> 0 then begin
			a[ai]:=dirs[result];
			inc(ai)
		end;
    result := n2dir[a[NeoIntRandom(ai)]];
end;

function Reverse(dir: byte): byte;
begin
    result := dir;
    if (dir and 3 <> 0) then result := result xor 3;
    if (dir and 12 <> 0) then result := result xor 12;
end;

function FollowPac(ghostNumber, mask: byte): byte;
var dx, dy: byte;
    bestdir1, bestdir2: byte;
begin
    g := ghosts[ghostNumber];
    if NeoIntRandom(g^.level) = 0 then exit(RandomDir(mask));
    dx := Abs(shortInt(pac.x - g^.x));
    dy := Abs(shortInt(pac.y - g^.y));
    if dy > dx then begin
        if g^.y < pac.y then bestdir1 := JOY_DOWN
            else bestdir1 := JOY_UP;
        if g^.x < pac.x then bestdir2 := JOY_RIGHT
            else bestdir2 := JOY_LEFT;
    end else begin
        if g^.x < pac.x then bestdir1 := JOY_RIGHT
            else bestdir1 := JOY_LEFT;
        if g^.y < pac.y then bestdir2 := JOY_DOWN
            else bestdir2 := JOY_UP;
    end;
    result := NeoIntRandom(4);  // how often choose second choice
    if result = 0 then begin
        result := bestdir2;
        bestdir2 := bestdir1;
        bestdir1 := result;
    end;

    if gameState = GAME_ESCAPE then begin
        bestdir1 := Reverse(bestdir1); // reverse directions when ghost escapes
        bestdir2 := Reverse(bestdir2);
    end;

    result := DIR_NONE;
    if (bestdir2 and mask) <> 0 then result := n2dir[bestdir2];
    if (bestdir1 and mask) <> 0 then result := n2dir[bestdir1];
    if result = DIR_NONE then result := RandomDir(mask);

end;

procedure MoveGhost(ghostNumber: byte);
var exitMask, exitCount, step: byte;
begin
    g := ghosts[ghostNumber];
    if (g^.deadCount > 0) then begin
        PaintOnTileset(g^.dx, g^.dy, g^.reward);
        g^.deadCount := g^.deadCount - 1;
        if g^.deadCount = 0 then begin
            PaintOnTileset(g^.dx, g^.dy, GetBoard(g^.dx, g^.dy));
            g^.dir := DIR_NONE;
            Dec(ghostCount);
        end;
    end else
    if (g^.dir <> DIR_NONE) then begin

        g^.sy := TOP_OFFSET + ((g^.y - viewportTop) * 8) - vscrollstep;
        g^.sx := g^.x shl 3;

        if Odd(g^.dir) then g^.sx := g^.sx + g^.step
        else g^.sy := g^.sy + g^.step;

        step:=1;
        if gameState = GAME_ESCAPE then step := frame and 1; // slow down by skipin steps in escape mode
        if step<>0 then begin
			if ((g^.dir = DIR_RIGHT) or (g^.dir = DIR_DOWN)) then g^.step := g^.step + step;
			if ((g^.dir = DIR_LEFT) or (g^.dir = DIR_UP)) then g^.step := g^.step - step;

			if Abs(g^.step) = 8 then begin // moved into new position
				if (Abs(shortInt(g^.y-pac.y))<GHOST_DESPAWN_DISTANCE) then g^.despawn := 0
				else g^.despawn := g^.despawn + 1;
				if g^.despawn = GHOST_DESPAWN_DELAY then DespawnGhost(ghostNumber)
				else begin
					if g^.step > 0 then g^.step := 1 else g^.step := -1;
					if Odd(g^.dir) then begin // horizontal
						g^.x := g^.x + g^.step;
						if not CanMoveTo(g^.x + g^.step,g^.y) then g^.dir := DIR_NONE; // on turns try to follow pac
					end else begin                // vertical
						g^.y := g^.y + g^.step;
						if not CanMoveTo(g^.x,g^.y + g^.step) then g^.dir := DIR_NONE; // on turns try to follow pac
					end;
					g^.step := 0;

					exitMask := GetNeighbours(g^.x, g^.y);
					exitCount := exitMask shr 4;
					 // if more than 3 exits or cannot continue move then take decision
					if (exitCount > 2) or (g^.dir = DIR_NONE) then g^.dir := FollowPac(ghostNumber,exitMask);
				end;
			end;
		end;
        ghostSpritesheets[ghostNumber] := ghost_sprites[g^.dir];
    end;
end;

procedure ShowGhost(ghostNumber: byte);
var ss: array[0..0] of byte;
    snum : byte;
begin
    g := ghosts[ghostNumber];
    if gameState = GAME_ESCAPE then begin
        ss := ghost_escape;
        snum := ss[ghostFrame];
    end else begin  
        ss := ghostSpritesheets[ghostNumber];
        snum := ss[ghostFrame]+16+(ghostNumber shl 2);
    end;
    if (g^.dir <> DIR_NONE) and (g^.deadCount = 0) then begin
           if (smallInt(g^.sy)>-8) and (smallInt(g^.sy)<120) then NeoUpdateSprite(ghostNumber+1,g^.sx shl 1 - 8,g^.sy shl 1,snum,0,7)
           else NeoHideSprite(ghostNumber+1);
    end;
end;

procedure MoveGhosts;
begin
    MoveGhost(0);
    MoveGhost(1);
    MoveGhost(2);
    MoveGhost(3);
end;

procedure ShowGhosts;
begin
    ShowGhost(0);
    ShowGhost(1);
    ShowGhost(2);
    ShowGhost(3);
    ghostFrame := (frame shr 4) and 1;
end;

function FreeGhostSlot:byte;
begin
    for i := 0 to GHOST_NUM - 1 do begin
        g := ghosts[i];
        if g^.dir = DIR_NONE then exit(i);
    end;
end;

function SelectSpawner: byte;
var i,min,dist:byte;
begin
    min:=100;
    for i:=0 to spawnerCount-1 do begin
        dist := Abs(shortint(Lo(spawners[i]) - pac.y));
        if dist < min then begin
            result := i;
            min := dist;
        end;
    end;
end;

procedure CloseAllSpawners;
begin
    for i := 0 to spawnerCount-1 do
        PaintOnTileset(Hi(spawners[i]),Lo(spawners[i]),TILE_SPAWNER);
end;

procedure TryToSpawnGhost;
var ghostNumber: byte;
begin
    if ghostCount < 4 then begin
        ghostNumber := FreeGhostSlot;
        if spawnCounter = 80 then begin
            spawnerActive := SelectSpawner;
            PaintOnTileset(Hi(spawners[spawnerActive]),Lo(spawners[spawnerActive]),TILE_SPAWNER_OPEN1);
        end;
        if spawnCounter = 40 then begin
            PaintOnTileset(Hi(spawners[spawnerActive]),Lo(spawners[spawnerActive]),TILE_SPAWNER_OPEN2);
        end;
        if (spawnCounter = 0) then begin 
			Inc(spawnCounter);
			if (pac.frame = ghostNumber + 1) then begin
				PaintOnTileset(Hi(spawners[spawnerActive]),Lo(spawners[spawnerActive]),TILE_SPAWNER);
				SpawnGhost(ghostNumber,Hi(spawners[spawnerActive]),Lo(spawners[spawnerActive]));
				spawnCounter := spawnDelay;
            end;
        end;
        Dec(spawnCounter);
    end;
end;


{********************************************** PAC ROUTINES ******************************************}


procedure InitPac(x, y: byte);
const PAC_START_Y = 7;
begin
    pac.x := x;
    pac.y := y;
    pac.step := 0;
    pac.frame := 0;
    pac.dir := DIR_NONE;
    pac_sprites[DIR_NONE] := pac_sprites[DIR_UP];
    pac.sprite := pac_sprites[pac.dir];
    viewportTop := pac.y - PAC_START_Y;
    if (pac.y > byte(boardLastLine - PAC_START_Y)) then viewportTop := viewportTop + (boardLastLine-PAC_START_Y-pac.y);
    if (pac.y < PAC_START_Y) then viewportTop := 0;
end;

procedure ShowPac;
begin
    NeoUpdateSprite(0,pac.sx shl 1 - 8,pac.sy shl 1,pac.sprite[pac.frame],pac_flip[pac.dir],7);
end;

procedure MovePac;
begin
    pac.sy := TOP_OFFSET + byte((pac.y - viewportTop) shl 3) - vscrollstep;  // count coordinates relative to viewport
    pac.sx := pac.x shl 3;

    if Odd(pac.dir) then pac.sx := pac.sx + pac.step  // increment coordinates by current pac step
    else pac.sy := pac.sy + pac.step;

    pac.frame:=(pac.frame + 1) and 7; // jump to next animation frame

    if ((pac.dir = DIR_RIGHT) or (pac.dir = DIR_DOWN)) then Inc(pac.step); // increment step for right or down movement
    if ((pac.dir = DIR_LEFT) or (pac.dir = DIR_UP)) then Dec(pac.step); // decrement for remaining directions

    if abs(pac.step) = 8 then begin // moved into new position
        if pac.step > 0 then pac.step := 1 else pac.step := -1;   // get position increment
        if Odd(pac.dir) then  // horizontal
            pac.x := pac.x + pac.step // increment absolute position
        else                // vertical
            pac.y := pac.y + pac.step; // increment absolute position
        pac.step := 0;
        CheckBoardAction;
    end;
end;

procedure CheckIfPacTurnsBack;
begin
    if _STICK0 <> 0 then begin    // if stick moved then check if pac wants turn back
        joy := _STICK0 and %00001111;
        if joy2dir[joy] = pac.dir then begin
            viewportMoveDir := -viewportMoveDir;
            pac.dir := (pac.dir + 2) and 3;
        end;
     end;
end;

procedure CheckIfPacTurns;
begin
    if (pac.dir <> DIR_NONE) then begin; // if pac was, moving check if it's able to continue movement
        case pac.dir of
            DIR_UP: if (not canMoveTo(pac.x, pac.y - 1)) then pac.dir := DIR_NONE;
            DIR_DOWN: if (not canMoveTo(pac.x, pac.y + 1)) then pac.dir := DIR_NONE;
            DIR_LEFT: if (not canMoveTo(pac.x - 1, pac.y)) then pac.dir := DIR_NONE;
            DIR_RIGHT: if (not canMoveTo(pac.x + 1, pac.y)) then pac.dir := DIR_NONE;
        end;
    end;

    if (joy) <> 0 then begin    // if stick is moved try to turn pac
                            // for diagonal stick movement, turning sides has bigger priority than fwd/bck movement
        dir := pac.dir;
        flag := pac.dir = DIR_NONE;
        if (joy and JOY_UP <> 0) then // up
            if canMoveTo(pac.x, pac.y-1) then
                if Odd(pac.dir) or flag then dir := DIR_UP;

        if (joy and JOY_DOWN <> 0) then // down
            if canMoveTo(pac.x, pac.y+1) then
                if Odd(pac.dir) or flag then dir := DIR_DOWN;

        if (joy and JOY_LEFT <> 0) then // left
            if canMoveTo(pac.x-1, pac.y) then
                if not Odd(pac.dir) or flag then dir := DIR_LEFT;

        if (joy and JOY_RIGHT <> 0) then // right
            if canMoveTo(pac.x+1, pac.y) then
                if not Odd(pac.dir) or flag then dir := DIR_RIGHT;
        pac.dir := dir;
    end;

    pac.sprite := pac_sprites[pac.dir];    // set sprite sheet based on direction
    pac_sprites[DIR_NONE] := pac.sprite;   // set current sprite sheet for idle
    viewportMoveDir := 0;                    // reset vscroll

    if (pac.dir = DIR_DOWN) and (pac.sy > 60) and (viewportTop + 14 < boardLastLine) then begin  // and up scroll needed
        viewportMoveDir := 1; // start scroll screen up
    end;

    if (pac.dir = DIR_UP) and (pac.sy < 50) and (viewportTop>0) then begin  // or scroll down needed
        viewportMoveDir := -1; // start scroll screen down
    end;

end;

function PacColider: byte;
var b,mask:byte;
begin
    mask := 1;
    result := 0;
    for b:=0 to 3 do begin
        g := ghosts[b];
        if (g^.dir <> DIR_NONE) and (g^.deadCount = 0) and (NeoInRange(0,b+1,9)<>0) then result := result or mask;
        mask := mask shl 1;
    end;
end;

function TouchedGhostNum(collider:byte):byte;
begin
    if (collider and %0001) <> 0 then result := 0;
    if (collider and %0010) <> 0 then result := 1;
    if (collider and %0100) <> 0 then result := 2;
    if (collider and %1000) <> 0 then result := 3;
end;

procedure PacDies;
begin
    Dec(lives);
    UpdateLives;
    frame := 1;
    pac.sprite := @pac_dies;
    pac.frame := 0;
end;

{********************************************** GAME ROUTINES ******************************************}

procedure ShowTitleScreen;
var selectState,selectState_:boolean;
	selectFrame:word;
    offt,offb,x:word;
    y:byte;
   
procedure DrawLogoLine;
begin
    for x:=0 to 319 do begin
        NeoSetColor(peek(offt));
        NeoDrawPixel(x,y+20);
        NeoSetColor(peek(offb));
        NeoDrawPixel(x,51-y+20);
        inc(offt);
        inc(offb);
    end;
    dec(offb,640);
end;

procedure ShowHiscore;
begin
    DrawRect(100,4,220,12,0);
    NeoStr(hiscore,s);
    s:=Concat('Hi-Score: ',s);
    DrawString(160-(Length(s)*3),4,1,4,s);
end;

begin

	selectFrame := 0;
    ClrScr;
    PlaySong(3);
    offt:=LOGO_ADDRESS;
    offb:=LOGO_ADDRESS+51*320;
    TileCol(1,$ee,false);
    TileCol(3,$28,false);
    TileCol(2,$84,false);
    TileCol(4,$04,false);
    TileCol(5,$0e,false);
    TileCol(6,$1e,false);
    Unapl(pointer(APU_LOGO_ADDRESS),pointer(LOGO_ADDRESS));

    for y:=0 to 26 do DrawLogoLine;
    
    s:='version for Neo6502';
    DrawString(106,4,1,4,s);

    for i:=0 to 3 do begin
        g := ghosts[i];
        g^.dir := DIR_RIGHT;
        g^.deadCount := 0;
        ghostSpritesheets[i] := ghost_sprites[DIR_RIGHT];
        g^.step := 0;
        g^.sy :=  48 + i * 12;
        g^.sx := -16;
    end;

    titleStage := 0;
    repeat
        
        feedSong;
        NeoWaitForVblank;
        inc(frame);
        ghostFrame := (frame shr 4) and 1;
        
        if selectFrame = 0 then begin 
            DrawRect(0,220,319,230,0);
            NeoStr(startingLevel+1,s);
            s:=Concat('BUTTON B - STARTING LEVEL: ',s);
            DrawString(78,220,1,6,s);
		end;
        
        if selectFrame = 150 then begin
            DrawRect(0,220,319,230,0);
            s:='BUTTON A - START THE GAME';
            DrawString(86,220,1,6,s);
        end; 

        if (selectFrame = 100) or (selectFrame = 250) then begin
            DrawRect(0,220,319,230,0);
        end;
		
		Inc(selectFrame);
		if selectFrame=300 then selectFrame:=0;
		
		
        case titleStage of
            0: begin
                for i:=0 to 3 do begin
                    g := ghosts[i];
                    g^.sx := g^.sx + 1;
                    if byte(g^.sx) = 63 then titleStage:=1;
                    ShowGhost(i);
                end;
            end;
            1: begin
                NeoSetDefaults(0,5,0,1,0);
                frame := 0;
                s:='"STINKY"';
                NeoDrawString(150,100,s);            
                s:='"KINKY"';
                NeoDrawString(150,124,s);            
                s:='"WANKY"';
                NeoDrawString(150,148,s);            
                s:='"ALAN"';
                NeoDrawString(150,172,s);            
                ShowHiscore;
                titleStage:=2;
            end;
            2: begin
                ShowGhosts;
                if frame = 0 then titleStage:=3;
            end;
            3: begin
                Drawrect(140,100,200,180,0);            
                titleStage:=6;
            end;
            6: begin
                for i:=0 to 3 do begin
                    g := ghosts[i];
                    g^.sx := g^.sx + 1;
                    if g^.sx > 190 then begin
                        titleStage:=7;
                        g^.sx := -16;
                    end;
                    ShowGhost(i);
                end;
            end;
            7: begin
                frame := 0;
                pac.step := 0;
                pac.frame := 0;
                pac.sprite := pac_sprites[DIR_RIGHT];
                pac.sy := 69;
                NeoSetDefaults(0,5,0,1,0);
                s:='code & gfx: bocianu';
                NeoDrawString(104,96,s);            
                s:='original music: LiSU';
                NeoDrawString(101,116,s);            
                s:='game written in Mad-Pascal';
                NeoDrawString(84,170,s);            
                s:='using sources from Atari XL/XE';
                NeoDrawString(72,190,s);            
                titleStage:=8;
            end;
            8: begin
                pac.frame:=(pac.frame + 1) and 7;
                pac.sx := frame-16;
                ShowPac;
                if frame > 190 then titleStage:=9;
            end;
            9: begin
                if frame = 200 then titleStage:=10;
            end;
            10: begin
                Drawrect(0,90,319,200,0);            
                titleStage:=0;
            end;

        end;

		selectState := FireBPressed;
		if (selectState_ <> selectState) and (selectState) then begin
			Inc(startingLevel);
			if startingLevel=levelCount then startingLevel := 0;
			selectFrame:=0;
		end;

        if keypressed then begin    
            k:=ReadKey;
            if (k='m') or (k='M') then music_on := not music_on;
        end;

		selectState_ := selectState;

    until FireAPressed; 
    repeat until not FireAPressed; 
    StopSong;
    HideSprites;
end;

procedure ShowLevelScreen;
var o:byte;
begin
    DrawRect(0,0,319,239,$a);
    s:='LEVEL';
    DrawString(22,40,10,0,s);
    DrawString(16,34,10,1,s);
    o:=136;
    NeoStr(currentLevel+1,s);
    if length(s)>1 then o:=106;
    DrawString(o+6,140,10,0,s);
    DrawString(o,134,10,1,s);
end;

procedure InitGameScreen;
begin
    NeoWaitForVblank;

    TileCol($07,$9a,false); // spawner doors
    TileCol($09,$96,false); // spawner frame
    TileCol($0c,$b6,false); // fruit green
    TileCol($0d,$2,false); // price/exit shadow
    
    TileCol($01,$26,false); // fruit face
    TileCol($0e,$2b,false); // fruit shine

    TileCol($0b,$18,false); // dot 
    TileCol($03,$1d,false); // dot light

    //TileCol(8,$84,true); // top-bottom

    DrawRect(0,0,319,239,8);
    ShowTopBar;
    UpdateScore;
    UpdateFood;
    UpdateLives;
end;

function ReadLevelData(ptr: word): word;
var tile,count,tileSize:byte;
    p:word;
begin
    tile := levelData[0];
    count := levelData[1];
    p := 2;
    tileSize := 2;

    case tile of

        TILE_DOT, TILE_EMPTY, TILE_VOID: begin
            tileSize := 4;
            repeat
                BoardRect(levelData[p], levelData[p+1], levelData[p+2], levelData[p+3], tile);
                p := p + tileSize;
                Dec(count)
            until count = 0;
        end;

        TILE_PILL: begin
            repeat
                SetBoard(levelData[p], levelData[p+1], tile);
                p := p + tileSize;
                Dec(count)
            until count = 0;
        end;

        TILE_SPAWNER: begin
            spawnerCount := 0;
            repeat
                SetBoard(levelData[p], levelData[p+1], tile);
                spawners[spawnerCount] := (levelData[p] shl 8) + levelData[p+1];
                p := p + tileSize;
                Inc(spawnerCount);
                Dec(count);
            until count = 0;
        end;

        TILE_WARP_LEFT: begin
            tileSize := 1;
            repeat
                SetBoard(0, levelData[p], TILE_WARP_LEFT);
                SetBoard(20, levelData[p], TILE_WARP_RIGHT);
                p := p + tileSize;
                Dec(count);
            until count = 0;
        end;

    end;
    result:=ptr+p;
end;

procedure LoadLevel(levelNum: byte);
var levelPtr: word;
begin
    ClearFeatures;
    levelPtr := levels[levelNum];
    Move(pointer(levelPtr), @level, SizeOf(level));
    spawnDelay := level.delay * 50;
    pillTime := level.pillTime * 50;
    levelPtr := levelPtr + SizeOf(level);
    levelData := pointer(levelPtr);
    Fillchar(@board, BOARDSIZE, TILE_VOID);
    repeat
        levelPtr := ReadLevelData(levelPtr);
        levelData := pointer(levelPtr);
    until levelData[0]=$ff;
end;

procedure InitGame;
begin
    lives := GAME_LIVES;
	//if cheat_mode then lives := 99;
    score := 0;
    liveBonus := 0;
    currentLevel := startingLevel;
    tmap.format := 1;
    tmap.width := BOARDWIDTH;
    tmap.height := BOARDHEIGHT;
    ClearGhosts;
end;

procedure InitView;
begin
    vscrollstep := 0;
    viewportMoveDir := 0;
    InitPac(level.startX, level.startY);
    spawnCounter := spawnDelay;
    gameState := GAME_NORMAL;
end;

procedure InitLevel(lvl: byte);
begin
    HideSprites;
    ShowLevelScreen;
    LoadLevel(lvl);

    SpriteCol(3,level.colors[3]);
    TileCol($04,level.colors[2],false); // back
    TileCol($0a,level.colors[4],false); // front
    TileCol($02,level.colors[1],false); // edge
    TileCol($08,level.colors[4],true); // top bottom bars
    TileCol($0f,level.colors[0],true); // top text color
    TileCol($05,level.colors[0],true); // top text color

    TileCol($06,level.colors[4],false); // exit
    
    PaintBoard;
    InitView;
    dotCount := dotCount - level.dotLimit;

    bonusPosPicked := false;
    bonusDelay := BONUS_DELAY * 50;

    frame:=0;
    repeat
        TileCol(1,frame,false);
        inc(frame);
        NeoWaitForVblank;
    until FireAPressed or (frame=60);

    DrawRect(0,0,319,239,$a);
    
    repeat until not FireAPressed;
end;

procedure GameRestore;
begin
    InitView;
    if bonusPosPicked then RemoveBonus;
    ClearGhosts;
    CloseAllSpawners;
    NeoWaitForVblank;
    PlaySong(0);
end;

procedure DrawTileMap;
begin
    NeoSelectTileMap(word(@tmap),8,(((viewportTop*8)+vscrollstep) shl 1)); 
    NeoDrawTileMap(0,TOP_OFFSET shl 1,320,236);
end;

procedure ShowGetReady;
var count:byte;
begin
    HideSprites;
    DrawTileMap;
    DrawRect(40,60,279,123,0);
    DrawRect(48,68,271,115,$a);
    PlaySong(2);
    frame := 0;
    count := 0;
    repeat
        Inc(frame);
        feedSong;
        if (frame and 7 = 0) then Inc(count);
        if (frame and 16 = 0) then begin
            DrawRect(48,68,271,115,$a);
        end else begin
            s:='ARE YOU'; 
            DrawStringSh(120,76,2,$f,s);
            s:='MAD ENOUGH?';
            DrawStringSh(96,94,2,$f,s);
        end;
        NeoWaitForVblank;
    until FireAPressed or (count=38); 
    StopSong;
    CheckBoardAction;
    PlaySong(0);
end;

procedure ShowGameOver;
var count:byte;
begin
    count:=0;
    DrawTileMap;
    HideSprites;

    if score >= hiscore then begin
		hiscore := score;
        StoreHiscore;
	end;

    DrawRect(72,60,247,192,0);
    DrawRect(80,68,239,186,$a);

    s:='HIGH SCORE'; 
    DrawStringSh(100,102,2,$3,s);
    NeoStr(hiscore,s);
    count := Length(s) * 6;
    DrawStringSh(160-count,120,2,$3,s);

    if score < hiscore then begin

        s:='YOUR SCORE'; 
        DrawStringSh(100,146,2,$f,s);
        NeoStr(score,s);
        count := Length(s) * 6;
        DrawStringSh(160-count,164,2,$f,s);

    end;

    frame := 0;
    repeat
        
        //if (score = hiscore) and (count<10) then
        if (frame and 8 = 0) then begin
            s:='GAME  OVER'; 
            DrawStringSh(100,76,2,$f,s);
        end else begin
            DrawRect(100,76,219,92,$a);
        end;

        NeoWaitForVblank;
        inc(frame);
    until FireAPressed; 
    repeat until not FireAPressed; 
end;

procedure LevelAdvance;
begin
    StopSong;
    Inc(currentLevel);
    if currentLevel = levelCount then begin     // end of levels
        level.colors[4] := 2;
        level.colors[1] := 6;
        gameState := GAME_OVER;
    end else begin      // jump to next level
        NeoWaitForVblank;
        InitLevel(currentLevel);
        InitGameScreen;
        ShowGetReady;
    end;
end;

{***************************************************************************************}
{************************************** MAIN *******************************************}
{***************************************************************************************}

begin
    Randomize;
    InitTables;
    music_on := true;
    LoadHiscore;
    SpriteCol(3,COLOR_PACMAD);
    SpriteCol($04,COLOR_ESCAPE);
    SpriteCol($01,COLOR_G1);
    SpriteCol($0b,COLOR_G2);
    SpriteCol($06,COLOR_G3);
    SpriteCol($0e,COLOR_G4);

    repeat

        ShowTitleScreen;
        InitGame;
        InitLevel(currentLevel);
        InitGameScreen;
        ShowGetReady;

        repeat          // ***************************** GAME LOOP

            //if music then msx.Play;
            inc(frame);
            feedSong;
            ReadStick;
            if keypressed then begin    
                k:=ReadKey;
                if (k='m') or (k='M') then music_on := not music_on;
            end;            

            case gameState of

                GAME_NORMAL, GAME_ESCAPE: begin   // NORMAL or ESCAPE
                    if gameState = GAME_ESCAPE then begin
                        if pillCounter < 150 then
                            if (pillCounter and 8) <> 0 then SpriteCol(4,COLOR_ESCAPE_BLINK)
                            else SpriteCol(4,COLOR_ESCAPE);
                        Dec(pillCounter);
                        if pillCounter = 0 then begin
                            PlaySong(0);
                            gameState := GAME_NORMAL;
                        end;
                    end;


                    MovePac;
                    MoveGhosts;

                    NeoWaitForVblank; // wait for VBLI

                    DrawTileMap;
                    ShowPac;
                    ShowGhosts;
                    MoveViewport;

                    CheckIfPacTurnsBack;

                    if pac.step = 0 then begin // pac enters new field
                        CheckIfPacTurns;
                    end;

                    collider := PacColider;       // detect collisions
                    if collider <> 0 then begin
                        if gameState = GAME_NORMAL then begin // pac dies
                            gameState := GAME_DEAD;
                            PlaySong(3);
                            PacDies;
                        end;
                        if gameState = GAME_ESCAPE then begin // ghost dies
                            if pillBonus < 500 then
                                pillBonus := pillBonus + 100;
                            AddScore(pillBonus);
                            Sfx(SFX_BONUS);
                            KillGhost(TouchedGhostNum(collider));
                        end;
                    end;

                    TryToSpawnGhost;

                end;

                GAME_WIN: begin   // REACHED LEVEL EXIT
                    if frame < 2 then begin
                        ClearGhosts;
                        PlaySong(3,64);
                    end;
                    level.colors[1] := ((frame shl 2) and %11110000) or 6;
                    pac.frame := 2;                 // spin pac
                    pac.dir := (frame shr 2) and 3;
                    pac.sprite := pac_sprites[pac.dir];
                    NeoWaitForVblank;
                    ShowPac;
                    if (frame = 172) or (FireAPressed) then LevelAdvance;      // animation ends
                end;

                GAME_DEAD: begin      // PAC DIED
                    if (pac.frame < 17) and ((frame and 3) = 0) then begin  // animate pac
                        Inc(pac.frame);
                        ShowPac;
                    end;
                    if frame = 180 then begin         // animation end
                        StopSong;
                        if lives = 0 then gameState := GAME_OVER
                        else GameRestore;
                    end;
                    NeoWaitForVblank;
                    MoveGhosts;
                    ShowGhosts;
                end;

            end;

            if bonusDelay>0 then Dec(bonusDelay);
            if not bonusPosPicked then PickBonusPos
            else begin
                if bonusCountdown>0 then begin
                    dec(bonusCountdown);
                    if bonusCountdown=0 then RemoveBonus;
                end;
                if (bonusDelay=0) and (bonusCountdown=0) then PlaceBonus;
            end;

            if dotCount = 0 then BlinkExit;

        until gameState = GAME_OVER;
        ShowGameOver;
        HideSprites;
    until FALSE;

end.



const
    BOARDWIDTH = 21;
    BOARDHEIGHT = 100;
    BOARDSIZE = BOARDWIDTH * BOARDHEIGHT;
    TOP_OFFSET = 6;

    PALETTE_ADDRESS = $9600;
    LEVELS_ADDRESS = $9900;
    APU_LOGO_ADDRESS = $AB00;
       
    LOGO_ADDRESS = $B000;
    BOARDBASE = $B000;
    TILESETBASE = $C000;

    DIR_UP = 0;
    DIR_RIGHT = 1;
    DIR_DOWN = 2;
    DIR_LEFT = 3;
    DIR_NONE = 4;

    JOY_LEFT = %0001;
    JOY_RIGHT = %0010;
    JOY_UP = %0100;
    JOY_DOWN = %1000;
    
    GAME_LIVES = 3;
    LIFE_BONUS = 10000;
    GHOST_NUM = 4;
    GHOST_DESPAWN_DISTANCE = 16;
    GHOST_DESPAWN_DELAY = 100;

    BONUS_DELAY = 20;
    BONUS_COUNTDOWN = 5;


    COLOR_PACMAD = $1c;
    COLOR_G1 = $26;
    COLOR_G2 = $4A;
    COLOR_G3 = $8C;
    COLOR_G4 = $18;
    COLOR_ESCAPE = $76;
    COLOR_ESCAPE_BLINK = $0F;

    SFX_BONUS = 21;
    SFX_LIFE = 8;
    SFX_EXIT = 5;

	// ai helper tables
    joy2dir : array [0..15] of byte = (8,1,3,8,2,8,8,8,0,8,8,8,8,8,8,8);
    n2dir : array [0..15] of byte   = (8,3,1,8,0,8,8,8,2,8,8,8,8,8,8,8);
    dirs : array [0..3] of byte = (4,8,1,2);

    // max number of board features (can be increased)
    SPAWNERS_NUM = 10;

type
    boardT = array [0..BOARDSIZE] of byte;

    TPalColor = record
        r:byte;
        g:byte;
        b:byte;
    end;

    gameStateT = (
        TITLE_SCREEN,
        GAME_START,
        GAME_NORMAL,
        GAME_ESCAPE,
        GAME_WIN,
        GAME_DEAD,
        GAME_OVER
    );

    ghostT = record
        x,y:byte;
        step:shortInt;
        dir:byte;
        level:byte;     // 1:always random    2:1/2 random     3:1/3 random...
        sx,sy:smallInt;
        dx,dy,deadCount,reward, despawn:byte;
    end;

    pacT = record
        x,y:byte;
        step:shortInt;
        frame:byte;
        dir:byte;
        sx,sy:smallInt;
        sprite: array[0..0] of byte;
    end;

    levelT = record
        startX,startY: byte;
        exitX, exitY: byte;
        ai: array [0..3] of byte;
        delay: byte;
        pillTime: byte;
        dotLimit: word;
        colors: array [0..4] of byte;
    end;


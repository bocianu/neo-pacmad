from PIL import Image
import os

fname = 'pacmadlogoneo.png'
outfile = 'logo.img'


print(f'*** procesing file {fname}')
img = Image.open(f'{fname}')
w, h = img.size
if img.mode != 'P':
    print(f'*** changing color depth')
    img = img.convert("P", palette=Image.ADAPTIVE, colors=256)        

pixels = bytearray(img.getdata())
palette = bytearray(img.getpalette())
if len(pixels)>0:
    with open(f'{outfile}', 'wb') as out_file:
        print(f'*** writing {fname}')
        out_file.write(pixels)
with open(f'{outfile}.pal', 'wb') as out_file:
    print(f'*** saving palette for {fname}')
    out_file.write(palette)
print(f'*** DONE\n')
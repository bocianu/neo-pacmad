
var 
    pac_moveh: array [0..7] of byte = (0, 1, 2, 3, 4, 3, 2, 1);
    pac_movev: array [0..7] of byte = (0, 5, 6, 7, 8, 7, 6, 5);
    pac_flip: array [0..3] of byte = (0,0,2,1);
    pac_dies: array [0..17] of byte = (0, 5, 6, 7, 8, 9, 10, 11, 12,
                                        13, 14, 15, 51, 52, 53, 54, 55, 56);

    ghost_down: array [0..1] of byte = (2, 18);
    ghost_up: array [0..1] of byte = (0, 16);
    ghost_left: array [0..1] of byte = (3, 19);
    ghost_right: array [0..1] of byte = (1, 17);
    ghost_escape: array [0..1] of byte = (48, 49);

var
    pac_sprites: array [0..4] of pointer = (@pac_movev, @pac_moveh, @pac_movev, @pac_moveh, @pac_movev);
    ghost_sprites: array [0..4] of pointer = (@ghost_up, @ghost_right, @ghost_down, @ghost_left, @ghost_escape);

    // fences: array [0..16] of byte = (
    //     $01,    // 0 none
    //     $01,    // 1 T
    //     $02,    // 2 B
    //     $02,    // 3 TB
    //     $01,    // 4 L
    //     $04,    // 5 TL
    //     $07,    // 6 BL
    //     $0b,    // 7 LTB
    //     $03,    // 8 R
    //     $05,    // 9 TR
    //     $06,    // 10 BR
    //     $09,    // 11 TBR
    //     $03,    // 12 LR
    //     $08,    // 13 TLR
    //     $0a,    // 14 BLR
    //     $0c,    // 15 LTBR
    //     $1d     // 16 rbCORNER

    // );

    fences: array [0..17] of byte = (
        $01,    // 0 none
        $01,    // 1 L
        $03,    // 2 R
        $03,    // 3 LR
        $01,    // 4 T
        $04,    // 5 TL
        $05,    // 6 TR
        $08,    // 7 TRL
        $02,    // 8 B
        $07,    // 9 BL
        $06,    // 10 BR
        $0a,    // 11 BLR
        $02,    // 12 BT
        $0b,    // 13 BTL
        $09,    // 14 BTR
        $0c,    // 15 LTBR
        $1d,    // 16 rbCORNER
        $1e    // 16 rbCORNER TL
    );


    tiles: array [0..19] of byte = (
        $01, // 0 void
        $0d, // 1 dot
        $00, // 2 empty
        $0e, // 3 pill
        $12, // 4 spawner
        $13, // 5 spawner open 1
        $14, // 6 spawner open 2
        $11, // 7 warp R
        $10, // 8 warp L
        $0f, // 9 exit
        $0f, // 10 exit_open

        $18, // 11 - 100
        $19, // 12 - 200
        $1a, // 13 - 300
        $1b, // 14 - 400
        $1c, // 15 - 500

        $15, // 16 - apple
        $16, // 17 - cherry
        $17, // 18 - banana
        1
        );
const
    TILE_VOID = 0;
    TILE_DOT = 1;
    TILE_EMPTY = 2;
    TILE_PILL = 3;
    TILE_SPAWNER = 4;
    TILE_SPAWNER_OPEN1 = 5;
    TILE_SPAWNER_OPEN2 = 6;
    TILE_WARP_RIGHT = 7;
    TILE_WARP_LEFT = 8;
    TILE_EXIT = 9;
    TILE_EXIT_OPEN = 10;

    TILE_100 = 11;
    TILE_200 = 12;
    TILE_300 = 13;
    TILE_400 = 14;
    TILE_500 = 15;

    TILE_BONUS  = 16;
    BONUS_COUNT = 3;
